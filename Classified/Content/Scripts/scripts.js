jQuery(document).ready(function($) {

	// VALIDATION
		var validator = $('form.validate');

		// Method price
		jQuery.validator.addMethod("checkprice", function(value, element) {
		    return this.optional(element) || !(/[\!\@\%\^\/#\\&\*\_\/=\\(\)\:\;\'\"\<\>\.\?\/\[\{\]\}\|\\`\~\|a-z]/i.test(value));
		},"* Number only");
		jQuery.validator.addMethod("checkphone", function(value, element) {
		    return this.optional(element) || !(/[\!\@\$\%\^\/#\\&\*\_\/=\\:\;\'\"\<\>\.\?\/\[\{\]\}\|\\`\~\|a-z]/i.test(value));
		},"* Number only");

    validator.validate({
			rules: {
			  Category: "required",
			  Make: "required",
			  Model: "required",
			  Price: {
			  	required: true,
			  	checkprice: true
			  },
			  Phone: {
			  	checkphone: true
			  },
			  feet: {
			  	number: true
			  },
			  inches: {
			  	number: true
			  },
			  cm: {
			  	number: true
			  },
			  Email: {
			    email: true
			  },
			  Description: "required",
			},
			messages: {
			  Category: "* Required",
			  Price: {
                required :"* Required",
                checkprice: "* Number only"
              },
			  Make: "* Required",
			  Model: "* Required",
			  Phone: "* Number only",
			  Email: "Please enter a E-Mail",
			  Description: "* Required",
			  Feet: "* Number only",
			  Inches: "* Number only",
			  cm: "* Number only",
			}
    });
	//================================

	// Length
		$('input[name=cm]').closest('div').hide();
		$('input[name=feet]').closest('div').show();
		$('input[name=inches]').closest('div').show();
		$('input[name=lengu]').change(function () {
			val = $(this).val();
			$('input[name=cm], input[name=feet], input[name=inches]').val('');
			if(val == 'metric') {
				$('input[name=cm]').closest('div').show();
				$('input[name=feet]').closest('div').hide();
				$('input[name=inches]').closest('div').hide();
			}
			else {
				$('input[name=cm]').closest('div').hide();
				$('input[name=feet]').closest('div').show();
				$('input[name=inches]').closest('div').show();
			}
		})
	//================================
});