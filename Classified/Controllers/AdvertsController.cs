﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Classified.DAL;
using Classified.Models;

namespace Classified.Controllers
{
    public class AdvertsController : Controller
    {
        private ClassifiedContext db = new ClassifiedContext();

        // GET: Adverts1
        public ActionResult Index()
        {
            var adverts = db.Adverts.Include(a => a.Category);
            return View(adverts.ToList());
        }

        // GET: Adverts1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advert advert = db.Adverts.Find(id);
            if (advert == null)
            {
                return HttpNotFound();
            }
            var arg = advert.getImages();
            ViewBag.images = arg;
            return View(advert);
        }

        // GET: Adverts1/Create
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategorySubCategoryName");
            return View();
        }

        // POST: Adverts1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Advert advert, FormCollection f)
        {
            
            string langu = f["lengu"];
            advert.BuyerPaymentOptions = f["BuyerPaymentOptions"];
            if (f["idFiles"] != "")
            {
                advert.images = f["idFiles"];
            }
            if(langu == "imprerial")
            {
                string feet = f["feet"]+" feet";
                string inchs = f["inches"] + " inches";
                string length = feet +" - " + inchs;
                advert.Length = length;
                advert.Size = length;
                if (Int32.Parse(f["feet"]) >0 && Int32.Parse(f["inches"])>0)
                {
                    ModelState["Size"].Errors.Clear();
                }
            }
            if (langu == "metric")
            {
                string cm = f["cm"] + " cm";
                advert.Length = cm;
                advert.Size = cm;
                if(Int32.Parse(f["cm"]) > 0)
                {
                    ModelState["Size"].Errors.Clear();
                }
            }
            if (ModelState.IsValid)
            {
                db.Adverts.Add(advert);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategorySubCategoryName", advert.CategoryID);
            return View(advert);
        }

        // GET: Adverts1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advert advert = db.Adverts.Find(id);
            if (advert == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategorySubCategoryName", advert.CategoryID);
            return View(advert);
        }

        // POST: Adverts1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AdvertID,CategoryID,CanonicalUrl,Url,Headline,Length,Size,Price,Make,Model,Year,Description,Name,Phone,Email,Suburb,State,BuyerPaymentOptions,DeliveryOptions")] Advert advert)
        {
            if (ModelState.IsValid)
            {
                db.Entry(advert).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategorySubCategoryName", advert.CategoryID);
            return View(advert);
        }

        // GET: Adverts1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advert advert = db.Adverts.Find(id);
            if (advert == null)
            {
                return HttpNotFound();
            }
            return View(advert);
        }

        // POST: Adverts1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Advert advert = db.Adverts.Find(id);
            db.Adverts.Remove(advert);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Upload()
        {
            var _path = "";
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var fileName = System.IO.Path.GetFileName(file.FileName);
                _path = "~/Images/" + fileName;
                var path = System.IO.Path.Combine(Server.MapPath("~/Images"), fileName);
                file.SaveAs(path);


            }
            return Json(_path);
         }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
