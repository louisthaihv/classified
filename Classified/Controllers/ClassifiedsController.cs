﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Classified.Controllers
{
    public class ClassifiedsController : Controller
    {
        // GET: Classifieds
        public ActionResult Index()
        {
            return View();
        }

        // GET: Classifieds/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Classifieds/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Classifieds/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Classifieds/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Classifieds/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Classifieds/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Classifieds/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
