﻿using System;
using System.Collections.Generic;
namespace Classified.Models
{
    public class Category
    {
        public int CategoryID { get; set; }
        public string CategorySubCategoryName { get; set; }  // e.g. Boats - Tinnies
        public string ParentCategoryName { get; set; }       // e.g. Boats
        public string SubCategoryName { get; set; }          // e.g. Tinnies

        public string ParentCategorySEOPath { get; set; }
        public string SubCategorySEOPath { get; set; }

        public string MakeType { get; set; }             // { List,Text,None } - None=hidden, Text=TextBox, List=use Makes list in dropdown
        public bool IsWanted { get; set; }               // When wanted category, don't display 'price' input/output

        public bool ShowLength { get; set; }             // Whether to show/hide on form
        public string MetricLengthUnit { get; set; }     // cm | metre
        public string ImperialLengthUnit { get; set; }   // foot

        public bool ShowSize { get; set; }               // Whether to show/hide on form
        public string SizeTitle { get; set; }            // Label for Size (e.g. 'Volume', 'Width', ...)
        public string MetricSizeUnit { get; set; }       // cm | metre
        public string ImperialSizeUnit { get; set; }     // foot | inches

        public virtual ICollection<Category> Categories { get; set; }
    }
}