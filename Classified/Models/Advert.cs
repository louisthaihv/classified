﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Classified.Models
{
   public class Advert
    {
        
        public int AdvertID { set; get; }
        [Required]
        public int CategoryID { get; set; }
        public string CanonicalUrl { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string Headline { get; set; }
        //[Required]
        [Display(Name = "Length number")]
        public string Length { get; set; }           // include units.  e.g. "30 feet"
        [Required]
        [Display(Name = "Size number")]
        public string Size { get; set; }             // include units.  e.g. "50 cm"
        [Required]
        public string Price { get; set; }
        [Required]
        public string Make { get; set; }
        [Required]
        public string Model { get; set; }
        public int Year { get; set; }             // For data entry, offer (Dates.Now.Year to Dates.Now.Year-20) as selection
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Advert Description")]
        public string Description{ get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string images { get; set; }
        /*
             BuyerPaymentOptions: Comma Separated Vaues of:
             ----------------------------------------------
                Cash
                Direct Deposit
                Credit Card
                PayPal
                Cheque
                Money Order/Bank Cheque
        */
        public string BuyerPaymentOptions { get; set; }

        /*
            DeliveryOptions: One of:
            ----------------------------------------------
                Local Pickup Only
                Local Pickup, or Post
                Post Only, no local pickup
                Not specified
        */
        public string DeliveryOptions { get; set; }

        public string[] getImages()
        {
            string[] images;
            if (this.images != "" && this.images!=null)
            {
                images = this.images.Split(',');
            }
            else
            {
                images = null;
            }
            return images;
        }
        public virtual Category Category { get; set; }
    }
}
