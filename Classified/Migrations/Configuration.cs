namespace Classified.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Classified.Models;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<Classified.DAL.ClassifiedContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Classified.DAL.ClassifiedContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            var Categories = new List<Category>
            {
                new Category { CategorySubCategoryName = "Boats - Tinnies",   ParentCategoryName = "Boats",
                    SubCategoryName = "Tinnies",
                    ParentCategorySEOPath = "ParentCategorySEOPath 1", SubCategorySEOPath="SubCategorySEOPath 1",
                    MakeType = "MakeType 1", IsWanted=true, ShowLength=true, MetricLengthUnit="MetricLengthUnit 1",
                    ImperialLengthUnit="ImperialLengthUnit 1", ShowSize = true, SizeTitle = "SizeTitle 1",
                    MetricSizeUnit = "MetricSizeUnit 1", ImperialSizeUnit = "ImperialSizeUnit 1", 
                },
            };
            Categories.ForEach(c => context.Categories.AddOrUpdate(p => p.CategorySubCategoryName, c));
            context.SaveChanges();

            var Adverts = new List<Advert>
            {
                new Advert {
                    CategoryID = 1, CanonicalUrl = "CanonicalUrl 1", Url="Url 1", Headline="Headline 1", Length ="Length  1",
                    Size="Size 1", Price ="Price 1", Make ="Make 1", Model="Model 1", Year = 2015,
                    Description = "Description  1", BuyerPaymentOptions = "BuyerPaymentOptions 1", DeliveryOptions = "DeliveryOptions 1"
                },
            };

            Adverts.ForEach(a => context.Adverts.AddOrUpdate(p => p.Model, a));
            context.SaveChanges();
        }
    }
}
