namespace Classified.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advert",
                c => new
                    {
                        AdvertID = c.Int(nullable: false, identity: true),
                        CategoryID = c.Int(nullable: false),
                        CanonicalUrl = c.String(),
                        Url = c.String(),
                        Title = c.String(),
                        Headline = c.String(),
                        Length = c.String(),
                        Size = c.String(nullable: false),
                        Price = c.String(nullable: false),
                        Make = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        Year = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        Name = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        Suburb = c.String(),
                        State = c.String(),
                        images = c.String(),
                        BuyerPaymentOptions = c.String(),
                        DeliveryOptions = c.String(),
                    })
                .PrimaryKey(t => t.AdvertID)
                .ForeignKey("dbo.Category", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategorySubCategoryName = c.String(),
                        ParentCategoryName = c.String(),
                        SubCategoryName = c.String(),
                        ParentCategorySEOPath = c.String(),
                        SubCategorySEOPath = c.String(),
                        MakeType = c.String(),
                        IsWanted = c.Boolean(nullable: false),
                        ShowLength = c.Boolean(nullable: false),
                        MetricLengthUnit = c.String(),
                        ImperialLengthUnit = c.String(),
                        ShowSize = c.Boolean(nullable: false),
                        SizeTitle = c.String(),
                        MetricSizeUnit = c.String(),
                        ImperialSizeUnit = c.String(),
                        Category_CategoryID = c.Int(),
                    })
                .PrimaryKey(t => t.CategoryID)
                .ForeignKey("dbo.Category", t => t.Category_CategoryID)
                .Index(t => t.Category_CategoryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Advert", "CategoryID", "dbo.Category");
            DropForeignKey("dbo.Category", "Category_CategoryID", "dbo.Category");
            DropIndex("dbo.Category", new[] { "Category_CategoryID" });
            DropIndex("dbo.Advert", new[] { "CategoryID" });
            DropTable("dbo.Category");
            DropTable("dbo.Advert");
        }
    }
}
