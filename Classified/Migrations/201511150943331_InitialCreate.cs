namespace Classified.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Advert", "Size", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Advert", "Size", c => c.String(nullable: false));
        }
    }
}
